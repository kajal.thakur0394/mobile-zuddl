import PasswordLogin from "../screenObjects/PasswordLogin.screen";

describe("My Login Demo", () => {
  let userEmail: string;
  let password: string;

  beforeEach(async () => {
    userEmail = "paritosh.saini@zuddl.com";
    password = "goodPassword@123";
  });

  it("LEAD-CAPTURE || Login using Password", async () => {
    await PasswordLogin.loginViaPassword(userEmail, password);
    await PasswordLogin.logout();
  });
});
