import { $ } from "@wdio/globals";
import { DataUtil } from "../../utils/dataUtil";
import locators from "../locators/demo.json";

class PasswordLogin {
  private platform: "android" | "ios" = DataUtil.getPlatform();

  get loginWithPasswordButton() {
    return $(locators.loginWithPasswordButtonLocator[this.platform]);
  }
  get userName() {
    return $(locators.emailInputBoxLocator[this.platform]);
  }
  get password() {
    return $(locators.passwordInputBoxLocator[this.platform]);
  }
  get login() {
    return $(locators.loginButtonLocator[this.platform]);
  }
  get settingsIcon() {
    return $(locators.settingsIconButtonLocator[this.platform]);
  }
  get logoutButton() {
    return $(locators.logoutButtonLocator[this.platform]);
  }
  get logoutConfirmation() {
    return $(locators.logoutConfirmationLocator[this.platform]);
  }

  async loginViaPassword(username: string, password: string) {
    try {
      await this.loginWithPasswordButton.click();
      await this.userName.setValue(username);
      await this.password.setValue(password);
      await this.login.click();
    } catch (error) {
      console.error("Error occurred during login via password:", error);
      throw error;
    }
  }

  async logout() {
    await this.settingsIcon.click();
    await this.logoutButton.click();
    await $(locators.logoutConfirmationLocator[this.platform]).click();
  }
}

export default new PasswordLogin();
