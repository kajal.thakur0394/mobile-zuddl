import { $ } from "@wdio/globals";
import { fetchOtpFromEmail } from "../../utils/emailUtils";
import { DataUtil } from "../../../portal/utils/dataUtil";
import locators from "../locators/demo.json";

class OtpLogin {
  private platform: "android" | "ios" = DataUtil.getPlatform();

  get userName() {
    return $(locators.emailInputBoxLocator[this.platform]);
  }
  get verificationCode() {
    return $(locators.getVerificationCodeButtonLocator[this.platform]);
  }
  get loginButton() {
    return $(locators.loginButtonLocator[this.platform]);
  }
  get invalidEmailError() {
    return $(locators.invalidEmailErrorLocator[this.platform]);
  }

  async invalidEmailLogin(username: string) {
    await this.userName.setValue(username);
    await this.verificationCode.click();
    await expect(this.invalidEmailError).toBeDisplayed();
  }

  async loginViaOtp(username: string) {
    await this.userName.setValue(username);
    console.log("Username ........", username);
    // expect(this.invalidEmailError).not.toBeDisplayed();
    await this.verificationCode.click();
    const otp = await fetchOtpFromEmail(username);
    console.log("###############");
    console.log("OTP ........", otp);
    console.log("###############");
    await this.fillInOtp(otp);
    await this.loginButton.click();
  }

  private async fillInOtp(otp: number) {
    let otpLength = 6;
    while (otpLength >= 1) {
      const CurrentCell = $(
        `//android.widget.EditText[@resource-id="com.zuddl.android.leadretrieval.staging:id/edit_password${otpLength}"]`
      );
      console.log("###############");
      console.log("CurrentCell ........", CurrentCell);
      console.log("###############");

      const otpDigit = otp % 10;
      otp = Math.floor(otp / 10);
      await CurrentCell.setValue(otpDigit.toString());

      --otpLength;
    }
  }
}

export default new OtpLogin();
