import { EMAIL_ID, EMAIL_DOMAIN } from "../../../constants/constants";
import LoginScreen from "../screenObjects/Login.screen";
import MultiEventScreen from "../screenObjects/MultiEvent.screen";
import MenuScreen from "../screenObjects/Menu.screen";
import { ScheduleController } from "../../../controllers/ScheduleController";
import { generateUniqueString } from "../../../controllers/BaseController";
import EventsController from "../../../controllers/EventsController";


describe("Multievent Functionality", () => {
  let password = "goodPassword@123";
  let eventId: string;
  let eventName: string;
  let scheduleServices: ScheduleController;
  let sessionId: string;
  let sessionName: string;
  let speakerEmail = `mobile.speaker+${generateUniqueString()}${EMAIL_DOMAIN}`;
  let attendeeEmail = `mobile.attendee+${generateUniqueString()}${EMAIL_DOMAIN}`;

  before(async () => {
    await LoginScreen.reLaunchApp();
    await LoginScreen.acceptIosAlert();

    [eventId, eventName] = await EventsController.createNewEvent({
      eventTitle: "MobileTestEvent",
    });
    await EventsController.enableOtpLogin(eventId);

    scheduleServices = new ScheduleController(eventId);
  });

  after(async () => {
    await EventsController.cancelEvent(eventId);
  });

  it("Invite Speaker and Attendee to the Event", async () => {
    await EventsController.inviteSpeaker(eventId, speakerEmail);
    await EventsController.inviteAttendee(eventId, attendeeEmail);
  });

  it("Verify Speaker is able to login to the app", async () => {
    await LoginScreen.loginViaOtp(speakerEmail);
    await MenuScreen.logout();
  });

  it("Verify Organiser is able to login to the app", async () => {
    await LoginScreen.loginViaOtp(EMAIL_ID);
  });

  // it("Verify user able to view list of events -> live, upcoming and completed", async () => {});

  it("Verify redirections to -> live, upcoming and completed sections", async () => {
    await MultiEventScreen.openLiveEvents();
  });

  it("Verify user able to search in live events", async () => {
    await MultiEventScreen.serchEventInLiveEvents(eventName);
  });

  // it("Verify user able to search in upcoming events", async () => {});

  // it("Verify user able to search in completed events", async () => {});

  // it("Verify type of event is visible over all the list of events 'virtual', 'hybrid' and 'in-person'", async () => {});

  // it("Verify events time date is displayed", async () => {});

  it("Verify user is able to open event and redirect", async () => {
    await MultiEventScreen.searchAndSelectEvent(eventName);
  });

  it("Verify exit event pop-up is displayed and user is able to cancel exit event", async () => {
    await MultiEventScreen.cancelExitEvent();
  });

  it("Verify user is able to move back to events page", async () => {
    await MultiEventScreen.exitEvent();
  });
});
