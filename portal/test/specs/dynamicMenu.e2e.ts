import { EMAIL_ID } from "../../../constants/constants";
import LoginScreen from "../screenObjects/Login.screen";
import MultiEventScreen from "../screenObjects/MultiEvent.screen";
import Menu from "../screenObjects/Menu.screen";
import EventsController from "../../../controllers/EventsController";
import { DynamicMenuController } from "../../../controllers/MenuController";
import allure from "@wdio/allure-reporter";
import { CommonUtils } from "../../../utils/commonUtils";

describe("Dynamic menu", () => {
  let eventId: string;
  let eventName: string;
  let dynamicMenuController: DynamicMenuController;

  before(async () => {
    await LoginScreen.reLaunchApp();
    await LoginScreen.acceptIosAlert();

    [eventId, eventName] = await EventsController.createNewEvent({
      eventTitle: CommonUtils.getRandomEventTitle(),
    });

    dynamicMenuController = new DynamicMenuController(eventId);
  });

  after(async () => {
    await EventsController.cancelEvent(eventId);
  });

  it.only("Verify user name/designation and profile picture are displayed on top and has an arrrow button displayed to redirect inside profile.", async () => {
    allure.addLink(
      "https://linear.app/zuddl/issue/QAT-1049/verify-user-namedesignation-and-profile-picture-are-displayed-on-top",
      "QAT-1049"
    );
    await allure.step(`User logs in via OTP`, async () => {
      await LoginScreen.loginViaOtp(EMAIL_ID);
    });
    await allure.step(
      `User searches and select the event->${eventName} and opens the Menu option`,
      async () => {
        console.log("event-name-->"+eventName);
        
        await MultiEventScreen.searchAndSelectEvent(eventName);
        await browser.pause(2000);
        await Menu.menuButton.click();
      }
    );
    await allure.step(`Then verifies user profile details`, async () => {
      await Menu.verifyUserProfileDetails(["name", "designation", "image"]);
    });
  });

  it.only("Verify on selecting Schedule, user is redirectng to schedule tab.", async () => {
    await allure.step(``, async () => {
      // const check=false;
      // await expect(check).toBeTruthy();
    });
  });

  it("Verify on selecting Agenda, user is redirectng to Agenda tab.", async () => {});

  it("Verify on selecting Session, user is redirectng to Session tab.", async () => {});

  it("Verify on selecting Sponsors, user is redirectng to Sponsors tab.", async () => {});
});
