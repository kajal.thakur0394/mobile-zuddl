import { EMAIL_ID } from "../../../constants/constants";
import LoginScreen from "../screenObjects/Login.screen";
import MultiEventScreen from "../screenObjects/MultiEvent.screen";
import ScheduleScreen from "../screenObjects/Schedule.screen";
import { ScheduleController } from "../../../controllers/ScheduleController";
import EventsController from "../../../controllers/EventsController";


describe("Multievent Functionality", () => {
  let password = "goodPassword@123";
  let eventId: string;
  let eventName: string;
  let scheduleServices: ScheduleController;
  let sessionId: string;
  let sessionName: string;

  before(async () => {
    await LoginScreen.reLaunchApp();
    await LoginScreen.acceptIosAlert();

    [eventId, eventName] = await EventsController.createNewEvent({
      eventTitle: "MobileTestEvent",
    });

    scheduleServices = new ScheduleController(eventId);
  });

  after(async () => {
    // await EventsService.cancelEvent(eventId);
  });

  it("Add schedule to the event.", async () => {
    [sessionId, sessionName] = await scheduleServices.addSessionInEvent({});
  });

  it("Login and select the Event", async () => {
    await LoginScreen.loginViaPassword(EMAIL_ID, password);
    await MultiEventScreen.searchAndSelectEvent(eventName);
  });

  it("Open Schedule screen and verify 'All sessions' and 'My Schedule' tabs are visible and accessible", async () => {
    await ScheduleScreen.openScheduleScreen();
    await ScheduleScreen.verifySessionTabsAreSwtichable();
  });

  it("Verify session is present", async () => {
    await ScheduleScreen.openScheduleScreen();
    await ScheduleScreen.verifySessionPresence(sessionName);
  });

  it("Verify if the user is able to add session to the 'My Schedule'", async () => {
    await ScheduleScreen.addSessionToMySchedule(sessionName);
  });

  it("Verify if the user is able to remove session from the 'My Schedule'", async () => {
    await ScheduleScreen.removeSessionFromMySchedule(sessionName);
  });
});
