import { EMAIL_ID } from "../../../constants/constants";
import Login from "../screenObjects/Login.screen";
import MenuScreen from "../screenObjects/Menu.screen";

describe("Login functionality", () => {
  let password = "goodPassword@123";
  let invalidUserEmail = "abc@xyz.com";

  beforeEach(async () => {
    await Login.acceptIosAlert();
  });

  afterEach(async () => {
    await Login.reLaunchApp();
  });

  //Test case on hold
  //iOS formatter
  // it("Verify when user selects terms and policy link, user is redirected to web screen.", async () => {
  //   await Login.verifyLinksOnOtpLoginPage();
  //   await Login.verifyLinksOnPasswordLoginPage();
  // });

  it("Login using Password", async () => {
    await Login.loginViaPassword(EMAIL_ID, password);
    await MenuScreen.logout();
  });

  it("Verify when user does not select any field, login button should be displayed as disabled.", async () => {
    await Login.verifyGetVerificationCodeButtonIsDisabled();
    await Login.verifyLoginButtonIsDisabled();
  });

  it("Verify when user enters invalid email id format or not registered email, error is displayed or not", async () => {
    await Login.verifyIncorrectCredentialsError("otp", invalidUserEmail);
    await Login.verifyIncorrectCredentialsError("password", invalidUserEmail);
  });

  it("Verify when user enters valid email id, a tick mark icon and login button is highlighted", async () => {
    await Login.correctEmailTickVerification("otp", EMAIL_ID);
    await Login.correctEmailTickVerification("password", EMAIL_ID);
  });

  // it("Verify if attendee/organiser runs out of resend OTP max times, user is displayed with appropriate message", async () => {});

  it("Verify when attendee/organiser enters invalid password , error is displayed", async () => {
    await Login.incorrectPasswordVerification(EMAIL_ID, "password");
  });

  // it("Verify user is displayed with an error when user tries to login with old password", async () => {});

  // it("Verify user is able to login with newly changed password", async () => {});

  it("Verify if user is able to change password using Forgot password", async () => {
    await Login.resetPassword(EMAIL_ID, password);
    await Login.reLaunchApp();
    await Login.acceptIosAlert();
    await Login.loginViaPassword(EMAIL_ID, password);
    await MenuScreen.logout();
  });
});
