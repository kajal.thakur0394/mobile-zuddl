import { $ } from "@wdio/globals";
import locators from "../locators/menuScreenLocators.json";
import { DynamicMenuPageNames } from "../../../enums/DynamicMenuEnums";
import { DataUtil } from "../../../utils/dataUtil";
import { DESIGNATION, IMAGE, USER_NAME } from "../../../constants/constants";
import { CommonUtils } from "../../../utils/commonUtils";

class MenuScreen {
  private platform: "android" | "ios" = DataUtil.getPlatform();

  get menuButton() {
    return $(locators.menuButtonLocator[this.platform]);
  }
  get menuLabel() {
    return $(locators.menuLabelLocator[this.platform]);
  }
  get myInbox() {
    return $(locators.myInboxLocator[this.platform]);
  }
  get myMeetings() {
    return $(locators.myMeetingsLocator[this.platform]);
  }
  get meetingsRequests() {
    return $(locators.meetingsRequestsLocator[this.platform]);
  }
  get privacySettings() {
    return $(locators.privacySettingsButtonLocator[this.platform]);
  }
  get logoutButton() {
    return $(locators.logoutButtonLocator[this.platform]);
  }
  get logoutConfirmation() {
    return $(locators.logoutConfirmationLocator[this.platform]);
  }
  get myEventLabel() {
    return $(locators.logoutConfirmationLocator[this.platform]);
  }
  get userName() {
    return $(
      CommonUtils.replacePlaceholders(locators.userNameLocator[this.platform], {
        x: USER_NAME,
      })
    );
  }
  get userDesignation() {
    return $(
      CommonUtils.replacePlaceholders(
        locators.designationLocator[this.platform],
        { x: DESIGNATION }
      )
    );
  }
  get userImage() {
    return $(
      CommonUtils.replacePlaceholders(locators.imageLocator[this.platform], {
        x: IMAGE,
      })
    );
  }

  async logout() {
    await this.menuButton.click();
    await this.logoutButton.click();
    if (this.platform === "ios") {
      console.log("#######################");
      console.log("####  iOS Logout   ####");
      console.log("#######################");
      try {
        await driver.acceptAlert();
      } catch (error) {
        console.log("Logout confirmation alert not present.");
      }
    } else if (this.platform === "android") {
      console.log("#######################");
      console.log("####  android Logout   ####");
      console.log("#######################");
      try {
        await this.logoutConfirmation.click();
      } catch (error) {
        console.log("Logout confirmation alert not present.");
      }
    }
  }

  async clickOnUserChoosenPage(userPage: DynamicMenuPageNames) {
    await this.menuButton.click();

    //this is to dynamically generate the locators for the page buttons under dynamic menu
    let pageButtonLocator: any;
    if (this.platform === "android") {
      pageButtonLocator = await $(``);
    } else if (this.platform === "ios") {
      pageButtonLocator = await $(
        `//XCUIElementTypeButton[@name="${userPage}"]`
      );
    }
    await pageButtonLocator.click();

    //this is to dynamically generate the locators for the page heading
    let pageLabelLocator: any;
    if (this.platform === "android") {
      pageLabelLocator = await $(``);
    } else if (this.platform === "ios") {
      pageLabelLocator = await $(
        `(//XCUIElementTypeStaticText[@name="${userPage}"])[2]`
      );
    }
    await pageLabelLocator.click();
  }

  async verifyUserProfileDetails(expectedElements: string[]) {
    for (const element of expectedElements) {
      await this.verifyDetail(element);
    }
  }

  async verifyDetail(expectedElement: string) {
    switch (expectedElement) {
      case "name":
        if (this.platform === "ios") {
          await expect(await this.userName.isExisting()).toBeTruthy();
        } else {
          let actualName = await this.userName.getText();
          await expect(USER_NAME).toEqual(actualName);
        }
        break;
      case "designation":
        if (this.platform === "ios") {
          await expect(await this.userDesignation.isExisting()).toBeTruthy();
        } else {
          let actualDesignation = await this.userDesignation.getText();
          await expect(DESIGNATION).toEqual(actualDesignation);
        }
        break;
      case "image":
        if (this.platform === "ios") {
          await expect(await this.userImage.isExisting()).toBeTruthy();
        } else {
          await expect(await this.userImage.isExisting()).toBeTruthy();
        }
        break;
    }
  }

  private async verifyElement(
    locator: WebdriverIO.Element,
    expectedValue: string
  ) {
    if (this.platform === "ios") {
      await expect(await locator.isExisting()).toBeTruthy();
    } else {
      let actualValue = await locator.getText();
      await expect(expectedValue).toEqual(actualValue);
    }
  }
}

export default new MenuScreen();
