import { $ } from "@wdio/globals";
import MultiEventLocators from "../locators/multieventLocators.json";
import HomeLocators from "../locators/homeLocators.json";
import { DataUtil } from "../../../utils/dataUtil";

class MutliEventScreen {
  private platform: "android" | "ios" = DataUtil.getPlatform();

  private get searchIcon() {
    return $(MultiEventLocators.searchLocator[this.platform]);
  }
  private get searchInput() {
    return $(MultiEventLocators.searchInputLocator[this.platform]);
  }
  private get enterButton() {
    return $(MultiEventLocators.enterLocator[this.platform]);
  }
  private get homeLabel() {
    return $(HomeLocators.homeLabelLocator[this.platform]);
  }
  private get liveEvents() {
    return $(HomeLocators.liveEventsLocator[this.platform]);
  }
  private get upcomingEvents() {
    return $(HomeLocators.upcomingEventsLocator[this.platform]);
  }
  private get completedEvents() {
    return $(HomeLocators.completedEventsLocator[this.platform]);
  }
  private get backButton() {
    return $(HomeLocators.backButtonLocator[this.platform]);
  }
  private get yesExitButton() {
    return $(HomeLocators.yesExitCofirmationLocaor[this.platform]);
  }
  private get cancelEventExitButton() {
    return $(HomeLocators.cancelConfirmationLocator[this.platform]);
  }

  async searchAndSelectEvent(eventName: string) {
    await browser.pause(2000);
    await this.searchEvent(eventName);
    await this.selectEvent(eventName);
  }

  private async searchEvent(eventName: string) {
    await this.searchIcon.click();
    await browser.pause(2000);
    await this.searchInput.setValue(eventName);
  }

  private async selectEvent(eventName: string) {
    let eventNameLocator;
    if (this.platform === "android") {
      eventNameLocator = await $(
        `//android.widget.TextView[@resource-id="com.zuddl.attendee.onsite.android.staging:id/txt_event_name" and @text="${eventName}"]`
      );
    } else {
      eventNameLocator = await $(`//XCUIElementTypeButton[@name="${eventName}"]`);
    }
    await eventNameLocator.click();
  }

  async serchEventInLiveEvents(eventName: string) {
    await this.liveEvents.click();
    await this.searchEvent(eventName);
    await this.backButton.click();
    await this.backButton.click();
  }

  async openLiveEvents() {
    await this.liveEvents.click();
    await this.backButton.click();
  }

  async exitEvent() {
    await this.backButton.click();
    await expect(this.yesExitButton.isDisplayed()).toBeTruthy();
    await this.yesExitButton.click();
  }

  async cancelExitEvent() {
    await this.backButton.click();
    await expect(this.cancelEventExitButton.isDisplayed()).toBeTruthy();
    await this.cancelEventExitButton.click();
  }

  async confirmExitEvnent() {
    await this.backButton.click();
    await expect(this.yesExitButton.isDisplayed()).toBeTruthy();
    await this.yesExitButton.click();
  }
}

export default new MutliEventScreen();
