import { $ } from "@wdio/globals";
import locators from "../locators/loginLocators.json";
import {
  fetchResetPasswordLink,
  fetchVerificationCode,
} from "../../../utils/emailUtils";
import { DataUtil } from "../../../utils/dataUtil";

class PasswordLogin {
  private platform: "android" | "ios" = DataUtil.getPlatform();

  get loginWithPasswordButton() {
    return $(locators.loginWithPasswordButtonLocator[this.platform]);
  }
  get loginWithPasswordLabel() {
    return $(locators.loginWithPasswordLabelLocator[this.platform]);
  }
  get userName() {
    return $(locators.emailInputBoxLocator[this.platform]);
  }
  get tick() {
    return $(locators.correctEmailTickLocator[this.platform]);
  }
  get password() {
    return $(locators.passwordInputBoxLocator[this.platform]);
  }
  get login() {
    return $(locators.loginButtonLocator[this.platform]);
  }
  get settingsIcon() {
    return $(locators.settingsIconButtonLocator[this.platform]);
  }
  get forgotPassword() {
    return $(locators.forgotPasswordLinkLocator[this.platform]);
  }
  get resetPasswordRequestLabel() {
    return $(locators.resetPasswordRequestLabelLocator[this.platform]);
  }
  get resetPasswordEmailInput() {
    return $(locators.resetPasswordEmailInputLocator[this.platform]);
  }
  get submitButton() {
    return $(locators.submitButtonLocator[this.platform]);
  }
  get browserTab() {
    return $(locators.tabBarLocator[this.platform]);
  }
  get urlInput() {
    return $(locators.urlInputLocator[this.platform]);
  }
  get newPasswordInput() {
    return $(locators.newPasswordInputLocator[this.platform]);
  }
  get confirmNewPasswordInput() {
    return $(locators.confirmNewPasswordInputLocator[this.platform]);
  }
  get enterButton() {
    return $(locators.enterLocator[this.platform]);
  }
  get successPopup() {
    return $(locators.successPopup[this.platform]);
  }
  get resetLinkSentPopup() {
    return $(locators.resetLinkSentPopup[this.platform]);
  }
  get enterValidEmailError() {
    return $(locators.enterValidEmailError[this.platform]);
  }
  get invalidEmailPassword() {
    return $(locators.invalidEmailOrPasswordErrorLocator[this.platform]);
  }
  get getVerificationCode() {
    return $(locators.getVerificationCodeButtonLocator[this.platform]);
  }
  get verificationCodeInput() {
    return $(locators.verificationCodeInputLocator[this.platform]);
  }
  get loginButton() {
    return $(locators.loginButtonLocator[this.platform]);
  }
  get invalidEmailError() {
    return $(locators.invalidEmailErrorLocator[this.platform]);
  }
  get termsAndPolicyLink() {
    return $(locators.termsOfUseLinkLocator[this.platform]);
  }

  async reLaunchApp() {
    await driver.reloadSession();
  }

  async closeApp() {
    await driver.closeWindow();
  }

  async invalidEmailLogin(username: string) {
    await this.userName.setValue(username);
    await this.getVerificationCode.click();
    await expect(this.invalidEmailError).toBeDisplayed();
  }

  async loginViaOtp(userEmail: string) {
    await this.userName.setValue(userEmail);
    await browser.pause(2000);
    await this.getVerificationCode.click();
    const otp = await fetchVerificationCode(userEmail);
    await browser.pause(2000);
    console.log("###############");
    console.log("OTP ........", otp);
    console.log("###############");
    await this.fillInOtp(otp);
    await this.loginButton.click();
  }

  private async fillInOtp(otp: string) {
    await this.verificationCodeInput.setValue(otp);
  }

  async loginViaPassword(username: string, password: string) {
    await this.loginWithPasswordButton.click();
    await this.userName.setValue(username);
    await this.password.setValue(password);
    await this.login.click();
  }

  async loginViaPasswordAndroid(username: string, password: string) {
    await this.loginWithPasswordButton.click();
    await this.userName.setValue(username);
    await this.password.setValue(password);
    await this.login.click();
  }

  async checkLoginButtonDisabled() {
    await this.loginWithPasswordButton.click();
    await expect(this.loginButton).toBeDisabled();
  }

  async resetPassword(emailId: string, newPassword: string) {
    await this.forgotPasswordFlow(emailId);
    const resetLink = await this.fetchResetLink(emailId);
    await this.setNewPassword(resetLink, newPassword);
  }

  private async forgotPasswordFlow(emailId: string) {
    await this.loginWithPasswordButton.click();
    await this.forgotPassword.click();
    if (this.platform === "android") {
      try {
        await $(
          '//android.widget.Button[@resource-id="com.android.chrome:id/signin_fre_dismiss_button"]'
        ).click();
      } catch (error) {
        console.log("Welcome screen not present.");
      }
    }
    await this.resetPasswordRequestLabel.click();
    await this.resetPasswordEmailInput.setValue(emailId);
    await this.submitButton.click();
    if (this.platform === "ios") await expect(this.successPopup).toExist();
  }

  private async fetchResetLink(emailId: string) {
    const resetLink = await fetchResetPasswordLink(emailId);
    console.log("######################");
    console.log("Reset Link ->", resetLink);
    console.log("######################");
    return resetLink;
  }

  private async setNewPassword(resetLink: string, newPassword: string) {
    await this.browserTab.click();
    await this.urlInput.setValue(resetLink);
    if (this.platform === "android") {
      await $(
        "//androidx.recyclerview.widget.RecyclerView/android.view.ViewGroup[1]"
      ).click();
    } else {
      await this.enterButton.click();
    }
    await this.newPasswordInput.setValue(newPassword);
    await this.confirmNewPasswordInput.setValue(newPassword);
    await this.submitButton.click();
    if (this.platform === "ios") await expect(this.successPopup).toExist();
  }

  async verifyIncorrectCredentialsError(
    loginType: "otp" | "password",
    username: string
  ) {
    if (loginType === "password") {
      await this.loginWithPasswordButton.click();
    }

    await this.userName.setValue(username);

    if (loginType === "otp") {
      await expect(await this.enterValidEmailError).toExist();
      await expect(this.getVerificationCode).toBeDisabled();
    } else {
      await expect(await this.enterValidEmailError).toExist();
      await expect(this.loginButton).toBeDisabled();
    }
  }

  async incorrectPasswordVerification(username: string, password: string) {
    await this.loginWithPasswordButton.click();
    await this.userName.setValue(username);
    await this.password.setValue(password);
    await this.loginButton.click();
    await expect(await this.invalidEmailPassword).toExist();
  }

  async verifyLinksOnOtpLoginPage() {
    await this.veriftyTermsAndPolicyLinks();
  }

  async verifyLinksOnPasswordLoginPage() {
    await this.loginWithPasswordButton.click();
    await this.veriftyTermsAndPolicyLinks();
  }

  async verifyLoginButtonIsDisabled() {
    await this.loginWithPasswordButton.click();
    await expect(this.loginButton).toBeDisabled();
  }

  async verifyGetVerificationCodeButtonIsDisabled() {
    await expect(this.getVerificationCode).toBeDisabled();
  }

  async verifyTickSignIsDisplayed(userId: string) {
    await this.loginWithPasswordButton.click();
    await this.loginButton.isDisplayed();
    await expect(this.loginButton.isEnabled()).toBe(false);
  }

  async correctEmailTickVerification(
    loginType: "otp" | "password",
    username: string
  ) {
    if (loginType === "password") {
      await this.loginWithPasswordButton.click();
    }
    await this.userName.setValue(username);
    await expect(this.tick).toExist();
  }

  private async veriftyTermsAndPolicyLinks() {
    await this.termsAndPolicyLink.click();
  }

  async acceptIosAlert() {
    if (this.platform === "ios") {
      try {
        await driver.acceptAlert();
      } catch (error) {
        console.log("Alert not present");
      }
    }
  }
}

export default new PasswordLogin();
