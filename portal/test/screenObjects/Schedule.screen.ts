import { $ } from "@wdio/globals";
import locators from "../locators/scheduleLocators.json";
import { selectByAttribute } from "webdriverio/build/commands/element";
import { DataUtil } from "../../../utils/dataUtil";

class ScheduleScreen {
  private platform: "android" | "ios" = DataUtil.getPlatform();

  private get scheduleScreen() {
    return $(locators.scheduleScreenLocator[this.platform]);
  }
  private get allSessionsTab() {
    return $(locators.allSessionLocator[this.platform]);
  }
  private get myScheduleTab() {
    return $(locators.myScheduleLocator[this.platform]);
  }
  private get sessionFilter() {
    return $(locators.filterLocator[this.platform]);
  }
  private get sessionSearch() {
    return $(locators.searchLocator[this.platform]);
  }
  private get sessionSearchInput() {
    return $(locators.searchInputLocator[this.platform]);
  }
  private get noSessionAvailable() {
    return $(locators.noSessionsAvailableMessageLocator[this.platform]);
  }
  private get addToMySchedule() {
    return $(locators.addToMySchedule[this.platform]);
  }
  private get addedToMySchedule() {
    return $(locators.addedToMySchedule[this.platform]);
  }
  private get spotsLeft() {
    return $(locators.spotsLeft[this.platform]);
  }
  private get peopleAttending() {
    return $(locators.peopleAttending[this.platform]);
  }
  private get sessionTime() {
    return $(locators.sessionTime[this.platform]);
  }
  private get sessionStatus() {
    return $(locators.sessionStatus[this.platform]);
  }
  private get backButton() {
    return $(locators.sessionStatus[this.platform]);
  }

  async openScheduleScreen() {
    await this.scheduleScreen.click();
  }

  async openAllSessionsTab() {
    await this.allSessionsTab.click();
  }

  async openMyScheduleTab() {
    await this.myScheduleTab.click();
  }

  async verifySessionTabsAreSwtichable() {
    await this.openScheduleScreen();

    await this.openAllSessionsTab();
    if (await driver.isElementSelected("All sessions")) {
      console.error("All sessions tab is not selected");
    }

    await this.openMyScheduleTab();
    if (await driver.isElementSelected("My Schedule")) {
      console.error("My Schedule tab is not selected");
    }
  }

  async verifySessionPresence(sessionName: string) {
    await this.openScheduleScreen();

    await this.sessionSearch.click();
    await this.sessionSearchInput.setValue(sessionName);

    let sessionNameLocator = await this.getSessionNameLocator(sessionName);
    await expect(sessionNameLocator.isDisplayed()).toBeTruthy();
    return sessionNameLocator;
  }

  async verifySessionPresenceInTab(tab: "all" | "my", sessionName: string) {
    await this.openScheduleScreen();

    if (tab === "all") await this.openAllSessionsTab();
    else if (tab === "my") await this.openMyScheduleTab();

    await this.sessionSearch.click();
    await this.sessionSearchInput.setValue(sessionName);

    let sessionNameLocator = await this.getSessionNameLocator(sessionName);
    await expect(sessionNameLocator.isDisplayed()).toBeTruthy();
  }

  async verifySessionDetails(sessionName: string) {
    const sessionNameLocator = await this.verifySessionPresence(sessionName);
    await sessionNameLocator.click();
  }

  async addSessionToMySchedule(sessionName: string) {
    await this.openScheduleScreen();

    await this.sessionSearch.click();
    await this.sessionSearchInput.setValue(sessionName);

    let sessionNameLocator = await this.getSessionNameLocator(sessionName);
    await expect(sessionNameLocator.isDisplayed()).toBeTruthy();

    await this.addToMySchedule.click();
    await expect(this.addedToMySchedule.isDisplayed()).toBeTruthy();

    await this.backButton.click();

    try {
      await this.myScheduleTab.click();
    } catch (error) {
      await this.backButton.click();
      await this.openScheduleScreen();
      await this.myScheduleTab.click();
    }

    await this.sessionSearch.click();
    await this.sessionSearchInput.setValue(sessionName);
    await expect(sessionNameLocator.isDisplayed()).toBeTruthy();
    await expect(this.addedToMySchedule.isDisplayed()).toBeTruthy();
  }

  async removeSessionFromMySchedule(sessionName: string) {
    await this.openScheduleScreen();

    let sessionNameLocator = await this.getSessionNameLocator(sessionName);

    await this.myScheduleTab.click();
    await this.sessionSearch.click();
    await this.sessionSearchInput.setValue(sessionName);
    await expect(sessionNameLocator.isDisplayed()).toBeTruthy();
    await expect(this.addedToMySchedule.isDisplayed()).toBeTruthy();
    await this.addedToMySchedule.click();
    await expect(sessionNameLocator.isDisplayed()).not.toBeTruthy();

    await this.backButton.click();

    try {
      await this.allSessionsTab.click();
    } catch (error) {
      await this.backButton.click();
      await this.openScheduleScreen();
      await this.allSessionsTab.click();
    }

    await this.sessionSearchInput.setValue(sessionName);
    await expect(sessionNameLocator.isDisplayed()).toBeTruthy();
    await expect(this.addToMySchedule.isDisplayed()).toBeTruthy();
  }

  private async getSessionNameLocator(sessionName: string) {
    let sessionNameLocator;
    if (this.platform === "android") {
      sessionNameLocator = await $(
        `#lyt_top #tv_session_name[text='${sessionName}']`
      );
    } else {
      sessionNameLocator = await $(``);
    }

    return sessionNameLocator;
  }
}

export default new ScheduleScreen();
