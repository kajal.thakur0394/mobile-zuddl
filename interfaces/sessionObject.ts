export interface SessionObject {
  day?: string;
  eventId: any;
  hiddenSegment?: boolean;
  refId?: string;
  description: string;
  startDateTime: any;
  endDateTime: any;
  speakers?: any;
  title: any;
  type: any;
  segmentFormat?: string;
  eventInPersonLocationId?: null;
  maxAvailableSpots?: null;
}
