export interface AddAttendeePayload {
  firstName: string;
  lastName: string;
  email: string;
  ticketTypeId: string | null;
  designation: string;
  company: string;
  accessGroups: [];
  canSendConfirmationMail: boolean;
}
