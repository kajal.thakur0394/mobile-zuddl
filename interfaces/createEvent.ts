export interface CreateEventParameters {
  eventTitle: string;
  eventDescription?: string;
  defaultSettings?: boolean;
  sameDayEvent?: boolean;
  timezone?: string;
  deltaStartDays?: number;
  deltaStartHours?: number;
  deltaStartMinutes?: number;
  deltaStartSeconds?: number;
  deltaEndDays?: number;
  deltaEndHours?: number;
  deltaEndMinutes?: number;
  deltaEndSeconds?: number;
  teamId?: string | null;
  isFlex?: boolean;
  eventFormat?: string;
  seriesEventId?: string | null;
}
