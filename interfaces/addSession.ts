export interface AddSessionPayload {
  title: string;
  description: string;
  hiddenSegment?: boolean;
  speakers?: string[];
  type?: string;
  startDateTime: string;
  endDateTime: string;
  refId?: any;
  eventId: string;
  day?: string;
  segmentFormat?: string;
  eventInPersonLocationId?: string | null;
  maxAvailableSpots?: number | null;
}
