export interface AddSpeakerPayload {
  firstName: string;
  lastName: string;
  email: string;
  ticketTagType?: string;
  picUrl?: string;
  designation?: string;
  organization?: string;
  bio?: string;
  accessGroups?: [];
  accessGroupMappings?: [];
  canSendConfirmationMail?: boolean;
  linkedInProfile?: string;
  twitterProfile?: string;
  facebookProfile?: string;
  websiteUrl?: string;
  company?: string;
}
