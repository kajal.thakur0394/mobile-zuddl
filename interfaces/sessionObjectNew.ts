export interface SessionObjectNew {
  title: string;
  description: string;
  hiddenSegment?: boolean;
  speakers?: string[];
  eventTagIds?: string[];
  type: string;
  startDateTime: any;
  endDateTime: any;
  refId?: any;
  eventId: string;
  day?: string;
}
