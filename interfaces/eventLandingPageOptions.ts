import EventEntryType from "../enums/EventEnums";

export interface EventLandingPageOptions {
  eventEntryType?: EventEntryType;
  isMagicLinkEnabled?: boolean;
  isGuestAuthEnabled?: boolean;
  speakerAuthOptions?: any;
  attendeeAuthOptions?: any;
  isSpeakermagicLinkEnabled?: any;
}
