import { ANDROID_APP_PATH, IOS_APP_PATH } from "../constants/pathconstants";

export const androidDeviceCapabilities = [
  {
    platformName: "Android",
    browserName: "appium",
    "appium:deviceName": "Pixel 8 API 34",
    "appium:platformVersion": "14",
    "appium:automationName": "UiAutomator2",
    "appium:app": ANDROID_APP_PATH,
    "appium:disableWindowAnimation": true,
    "appium:autoGrantPermissions": true,
  },
];

export const iosDeviceCapabilities = [
  {
    platformName: "IOS",
    browserName: "appium",
    "appium:deviceName": "iPhone 15 Pro",
    "appium:platformVersion": "17.5",
    "appium:automationName": "XCUItest",
    "appium:app": IOS_APP_PATH,
  },
];
