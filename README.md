# Mobile Automation Project

## Introduction
This repository contains automated test scripts for mobile applications developed for both iOS and Android platforms.

#### The automation framework utilizes the following technologies:
>[Appium](https://appium.io/): An open-source tool for automating mobile applications.

>[WebdriverIO](https://webdriver.io/): A Next-gen browser browser and mobile automation test framework for Node.js.

>[TypeScript](https://www.typescriptlang.org/): A strongly typed programming language that builds on JavaScript by adding static type definitions.

>[Mocha](https://mochajs.org/): A feature-rich JavaScript test framework running on Node.js making asynchronous testing simple and fun.

>[Page Object Model (POM)](https://www.selenium.dev/documentation/test_practices/encouraged/page_object_models/): A design pattern for creating an object repository for web UI elements to increase test maintainability and reduce code duplication.


## Setup

### Prerequisites
1. **[Node.js](https://nodejs.org/)**: WebDriverIO is built on top of Node.js, providing a JavaScript framework for automation. Additionally, Appium itself is a Node.js server that exposes a REST API, allowing WebDriverIO to interact with mobile devices or emulators via the Appium server.
2. **[Java JDK](https://www.oracle.com/in/java/technologies/downloads/)**: Appium relies on the Android Debug Bridge (ADB) to interact with Android devices or emulators, and ADB requires Java to run.
3. **[Xcode](https://developer.apple.com/xcode/)**: Xcode provides essential tools and libraries necessary for building, debugging, and testing iOS applications. Appium interacts with iOS devices and simulators through Xcode's command-line tools and libraries, enabling actions such as installing, launching, and interacting with iOS applications during automation testing. Additionally, Xcode includes simulators that emulate different iOS device configurations, allowing testers to run tests on various iOS versions and screen sizes.
4. **[Android Studio](https://developer.android.com/studio)**: It provides essential tools like the Android SDK, emulator management, and ADB.


## Steps
### 1.  Make sure the node 18 is installed on system
To check the node version, use command:
```node -v```

To downgrade the node version, refer to this [link](https://stackoverflow.com/questions/47008159/how-to-downgrade-to-a-previous-node-version)

### 2. Clone Repository
Clone this repositroy to your local machine:
```git clone https://github.com/zuddl/mobile-qa.git```

### 3. Install Dependencies
Install project dependencies using npm:
```npm install```

# Incase you want to setup the appium + wdio project from scratch, use the following steps:
Follow the setup for the official website of [WebDriver.io](https://webdriver.io/docs/gettingstarted/)
else
Follow these steps:
##### Use this command to start the setup using wdio
> npm init wdio@latest .

###### Following confirmation message should be displayed
```
Need to install the following packages:
  create-wdio@8.4.7
Ok to proceed? (y) y

                 -:...........................-:.
                 +                              +
              `` +      `...`        `...`      + `
            ./+/ +    .:://:::`    `::///::`  ` + ++/.
           .+oo+ +    /:+ooo+-/    /-+ooo+-/ ./ + +oo+.
           -ooo+ +    /-+ooo+-/    /-+ooo+-/ .: + +ooo.
            -+o+ +    `::///:-`    `::///::`    + +o+-
             ``. /.     `````        `````     .: .``
                  .----------------------------.
           `-::::::::::::::::::::::::::::::::::::::::-`
          .+oooo/:------------------------------:/oooo+.
      `.--/oooo-                                  :oooo/--.`
    .::-``:oooo`                                  .oooo-``-::.
  ./-`    -oooo`--.: :.--                         .oooo-    `-/.
 -/`    `-/oooo////////////////////////////////////oooo/.`    `/-
`+`   `/+oooooooooooooooooooooooooooooooooooooooooooooooo+:`   .+`
-/    +o/.:oooooooooooooooooooooooooooooooooooooooooooo:-/o/    +.
-/   .o+  -oooosoooososssssooooo------------------:oooo- `oo`   +.
-/   .o+  -oooodooohyyssosshoooo`                 .oooo-  oo.   +.
-/   .o+  -oooodooysdooooooyyooo` `.--.``     .:::-oooo-  oo.   +.
-/   .o+  -oooodoyyodsoooooyyooo.//-..-:/:.`.//.`./oooo-  oo.   +.
-/   .o+  -oooohsyoooyysssysoooo+-`     `-:::.    .oooo-  oo.   +.
-/   .o+  -ooooosooooooosooooooo+//////////////////oooo-  oo.   +.
-/   .o+  -oooooooooooooooooooooooooooooooooooooooooooo-  oo.   +.
-/   .o+  -oooooooooooooooooooooooooooooooooooooooooooo-  oo.   +.
-+////o+` -oooo---:///:----://::------------------:oooo- `oo////+-
+ooooooo/`-oooo``:-```.:`.:.`.+/-    .::::::::::` .oooo-`+ooooooo+
oooooooo+`-oooo`-- `/` .:+  -/-`/`   .::::::::::  .oooo-.+oooooooo
+-/+://-/ -oooo-`:`.o-`:.:-````.:    .///:``````  -oooo-`/-//:+:-+
: :..--:-:.+ooo+/://o+/-.-:////:-....-::::-....--/+ooo+.:.:--.-- /
- /./`-:-` .:///+/ooooo/+///////////////+++ooooo/+///:. .-:.`+./ :
:-:/.           :`ooooo`/`              .:.ooooo :           ./---
                :`ooooo`/`              .:.ooooo :
                :`ooooo./`              .:-ooooo :
                :`ooooo./`              .:-ooooo :
            `...:-+++++:/.              ./:+++++-:...`
           :-.````````/../              /.-:````````.:-
          -/::::::::://:/+             `+/:+::::::::::+.
          :oooooooooooo++/              +++oooooooooooo-
 
                           Webdriver.IO
              Next-gen browser and mobile automation
                    test framework for Node.js


Installing @wdio/cli to initialize project...
✔ Success!
```

##### And following Configuration Wizard should be popped after that...
##### Type 'yes(y)' is the path is correct
```
===============================
🤖 WDIO Configuration Wizard 🧙
===============================

? A project named "mobile-automation-project" was detected at "/Users/path/to/mobile-qa", correct? (Y/n) 
```

##### As wdio supports Mobile, Desktop as well as Browser testing, and in our case we are targetting the Mobile app testing.
##### Select the first option
```
? What type of testing would you like to do? (Use arrow keys)
❯ E2E Testing - of Web or Mobile Applications 
  Component or Unit Testing - in the browser
    > https://webdriver.io/docs/component-testing 
  Desktop Testing - of Electron Applications
    > https://webdriver.io/docs/desktop-testing/electron 
  Desktop Testing - of MacOS Applications
    > https://webdriver.io/docs/desktop-testing/macos 
(Move up and down to reveal more choices) 
```

##### Currently we are working on local machine.
##### Select 'On my local machine' option
```
? Where is your automation backend located? (Use arrow keys)
❯ On my local machine 
  In the cloud using Experitest 
  In the cloud using Sauce Labs 
  In the cloud using BrowserStack 
  In the cloud using Testingbot or LambdaTest or a different service 
  I have my own Selenium cloud  
```

##### As we are targetting the mobile apps in this project.
##### Select 'Mobile - native, hybrid and mobile web apps, on Android or iOS' option
```
? Which environment you would like to automate? (Use arrow keys)
❯ Web - web applications in the browser 
  Mobile - native, hybrid and mobile web apps, on Android or iOS
```

##### Select the target platform of your choice. 
###### During this setup wizard, we can only select one platform at a time, but other platform can be added later
```
? Which mobile environment you'ld like to automate? (Use arrow keys)
❯ Android - native, hybrid and mobile web apps, tested on emulators and real devices
    > using UiAutomator2 (https://www.npmjs.com/package/appium-uiautomator2-driver) 
  iOS - applications on iOS, iPadOS, and tvOS
    > using XCTest (https://appium.github.io/appium-xcuitest-driver) 
```

##### Currently we are using Mocha framework.
```
? Which framework do you want to use? (Use arrow keys)
❯ Mocha (https://mochajs.org/) 
  Mocha with Serenity/JS (https://serenity-js.org/) 
  Jasmine (https://jasmine.github.io/) 
  Jasmine with Serenity/JS (https://serenity-js.org/) 
  Cucumber (https://cucumber.io/) 
  Cucumber with Serenity/JS (https://serenity-js.org/) 
```

##### Currently we are using TypeScript language.
```
? Do you want to use a compiler? (Use arrow keys)
  Babel (https://babeljs.io/) 
❯ TypeScript (https://www.typescriptlang.org/) 
  No!
```

##### Select 'n' for the autogenerated test files.
```
? Do you want WebdriverIO to autogenerate some test files? (Y/n) 
```

##### You can select one or more reporters, currently we are using 'spec' reporter.
```
? Which reporter do you want to use? (Press <space> to select, <a> to toggle all, <i> to invert selection, and <enter> to proceed)
❯◉ spec
 ◯ dot
 ◯ junit
 ◯ allure
 ◯ sumologic
 ◯ concise
 ◯ json
 ◯ reportportal
 ◯ video
 ◯ cucumber-json
 ◯ mochawesome
 ◯ timeline
 ◯ html-nice
 ◯ slack
 ◯ delta
 ◯ testrail
 ◯ light
(Move up and down to reveal more choices)
```

##### Add wait-for plugin.
```
? Do you want to add a plugin to your test setup? (Press <space> to select, <a> to toggle all, <i> to invert selection, and <enter> to proceed)
❯◯ wait-for: utilities that provide functionalities to wait for certain conditions till a defined task is complete.
   > https://www.npmjs.com/package/wdio-wait-for
 ◯ angular-component-harnesses: support for Angular component test harnesses
   > https://www.npmjs.com/package/@badisi/wdio-harness
 ◯ Testing Library: utilities that encourage good testing practices laid down by dom-testing-library.
   > https://testing-library.com/docs/webdriverio-testing-library/intro
```

##### Select 'appium' service.
```
? Do you want to add a service to your test setup? (Press <space> to select, <a> to toggle all, <i> to invert selection, and <enter> to proceed)
 ◉ appium
 ◯ visual
 ◯ vite
 ◯ nuxt
 ◯ firefox-profile
 ◯ gmail
 ◯ sauce
(Move up and down to reveal more choices)
```

##### If missed, execute it now.
```
? Do you want me to run `npm install` (Y/n) 
```

##### If done correctly, you will get the following message.
```
Run `npm audit` for details.
✔ Success!

Creating a WebdriverIO config file...
✔ Success!

Adding "wdio" script to package.json
✔ Success!

🤖 Successfully setup project at /Users/path/to/mobile-qa 🎉

Join our Discord Community Server and instantly find answers to your issues or queries. Or just join and say hi 👋!
  🔗 https://discord.webdriver.io

Visit the project on GitHub to report bugs 🐛 or raise feature requests 💡:
  🔗 https://github.com/webdriverio/webdriverio

To run your tests, execute:
$ cd /Users/paritosh/Developer/mobile-qa
$ npm run wdio
```

##### After setup completion, you will be prompted to setup Appium using appium-installer, press 'Y'.
```
? Continue with Appium setup using appium-installer (https://github.com/AppiumTestDistribution/appium-installer)? (Y/n) 
```

##### Appium Installer can be used to setup the environment for both iOS and Android testing.
```
👋 Hello, Appium user ✨

? Select an option (Use arrow keys)
❯ Need help setting up Android Environment to run your Appium test? 
  Need help setting up iOS Environment to run your Appium test? 
  Install Appium Server 
  Install Appium Drivers 
  Install Appium Plugin 
  Run Appium Doctor 
  Launch Emulators/Simulators
  Exit
  (Move up and down to reveal more choices)
```

###### Select the relevant options for your desired setup, else if you want to check the current setup status for iOS or Android setup, run "Appium Doctor"
Appium doctor tool helps with checking the current setup status and helps with any missing dependency or tool required for Appium automation.
```
? Select an option Run Appium Doctor
? platform: (Use arrow keys)
❯ android
  ios 
  dev 
```


-------------------------------------

## Test Execution
#### Important Files for execution...

##### config -> capabilities.ts
> add the desired device config in this file

##### static -> pathconstants.ts
> this file contains the app paths for both android and ios apps to be tested


### To run the test cases use the following command ...
``` npm run test ```

-------------------------------------
