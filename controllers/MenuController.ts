import { AxiosResponse } from "axios";
import { DynamicMenuPageNames } from "../enums/DynamicMenuEnums";
import BaseAPIController from "./BaseController";

export class DynamicMenuController extends BaseAPIController {
  eventId: string;
  constructor(eventId: string) {
    super();
    this.eventId = eventId;
  }
  async fetchPortalAppPages(portalId: string) {
    try {
      const getPagesUri = `/portal/${this.eventId}/pages?portalId=${portalId}`;
      const response: AxiosResponse = await this.get(getPagesUri);
      if (response.status !== 200) {
        throw new Error(
          `API call to get portal app pages failed with ${response.status}`
        );
      }
      console.log("#######################");
      console.log("Portal app Pages details -> ", response.data);
      console.log("#######################");
      return response.data;
    } catch (error) {
      console.error("Error fetching pages details for portal app - ", error);
      throw error;
    }
  }
  async fetchPortalAppNavigationDetails(portalId: string) {
    try {
      const getNavigationDetailsUri = `/portal/${this.eventId}/${portalId}/navigation/all`;
      const response: AxiosResponse = await this.get(getNavigationDetailsUri);
      if (response.status !== 200) {
        throw new Error(
          `API call to get portal app navigation details failed with ${response.status}`
        );
      }
      console.log("#######################");
      console.log("Portal app Navigation details -> ", response.data);
      console.log("#######################");
      return response.data;
    } catch (error) {
      console.error(
        "Error fetching navigation details for portal app - ",
        error
      );
      throw error;
    }
  }
  async fetchMenuSectionDetails(portalId: string) {
    try {
      const response = await this.fetchPortalAppNavigationDetails(portalId);
      return response.data.menuSections;
    } catch (error) {
      console.error("Error fetching menu section details - ", error);
      throw error;
    }
  }
  async fetchTabSectionDetails(portalId: string) {
    try {
      const response = await this.fetchPortalAppNavigationDetails(portalId);
      return response.data.tabSections;
    } catch (error) {
      console.error("Error fetching tab section details - ", error);
      throw error;
    }
  }
  async fetchPageId(portalId: string, pageName: DynamicMenuPageNames) {
    try {
      const pageList = await this.fetchPortalAppPages(portalId);
      let pageId;
      for (let page of pageList) {
        if (page.pageName === pageName) {
          pageId = page.pageId;
        }
      }
      return pageId;
    } catch (error) {
      console.error("Error fetching the page id - ", error);
      throw error;
    }
  }
  async addItemToMenu(portalId: string, pageName: DynamicMenuPageNames) {
    try {
      const pageId = await this.fetchPageId(portalId, pageName);
      const endPoint = `/portal/${this.eventId}/${portalId}/navigation/item/${pageId}`;
      const response: AxiosResponse = await this.post(endPoint);
      if (response.status !== 200) {
        `API call to add page to dynamic menu failed with ${response.status}`;
        throw new Error();
      }
    } catch (error) {
      console.error("Error adding page to dynamic menu - ", error);
      throw error;
    }
  }
  async fetchMenuItemId(portalId: string, pageName: DynamicMenuPageNames) {
    try {
      const menuDetails = await this.fetchMenuSectionDetails(portalId);
      const menuItemDetails = await menuDetails.navigationItems;
      for (let item of menuItemDetails) {
        if (item.name === pageName) {
          return item.id;
        }
      }
    } catch (error) {
      console.error("Error fetching the navigation item id - ", error);
      throw error;
    }
  }
  async deleteItemFromMenu(portalId: string, pageName: DynamicMenuPageNames) {
    try {
      const itemId = await this.fetchMenuItemId(portalId, pageName);
      const endPoint = `/portal/${this.eventId}/${portalId}/navigation/item/${itemId}`;
      const response: AxiosResponse = await this.delete(endPoint);
      if (response.status !== 200) {
        `API call to delete and item form dynamic menu failed with ${response.status}`;
        throw new Error();
      }
    } catch (error) {
      console.error("Error deleting item from dynamic menu - ", error);
      throw error;
    }
  }
  async reOrderItems(
    portalId: string,
    itemName: DynamicMenuPageNames,
    newSection: "nav" | "menu",
    orderNumber: number,
    itemType: "TABBAR" | "MENUBAR"
  ) {
    try {
      const parentId = "";
      const itemId = await this.fetchMenuItemId(portalId, itemName);
      const payload = {
        currentOrder: 3,
        newOrder: orderNumber,
        itemId: itemId,
        currentParent: "6c60a8e9-3bdf-4a43-92db-f173df78bd88",
        newParent: parentId,
        newParentType: "SECTION",
        newItemType: itemType,
      };
      const reorderEndPoint = `/portal/${this.eventId}/${portalId}/navigation/item/reOrder`;
      const response: AxiosResponse = await this.post(reorderEndPoint, payload);
      if (response.status !== 200) {
        `API call to reorder the items failed with ${response.status}`;
        throw new Error();
      }
    } catch (error) {
      console.error("Error reordering item - ", error);
      throw error;
    }
  }
}