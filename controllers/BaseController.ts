import { eventsAxiosClient, setAuthHeaderToClients } from "./axios";

class BaseAPIController {
  constructor() {
    setAuthHeaderToClients(process.env.AUTH_TOKEN);
  }

  async get(endpoint: string): Promise<any> {
    return eventsAxiosClient.get(endpoint);
  }

  async post(endpoint: string, data?: any): Promise<any> {
    return eventsAxiosClient.post(endpoint, data);
  }

  async patch(endpoint: string, data: any): Promise<any> {
    return eventsAxiosClient.patch(endpoint, data);
  }

  async delete(endpoint: string): Promise<any> {
    return eventsAxiosClient.delete(endpoint);
  }
}
export default BaseAPIController;

export function generateUniqueString() {
  const timestamp = Date.now().toString(36);
  const randomStr = Math.random().toString(36).substring(2, 5); // Adjust length as needed
  return `${timestamp}${randomStr}`;
}
