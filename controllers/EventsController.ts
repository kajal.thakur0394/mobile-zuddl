import { AxiosResponse } from "axios";
import createEventPayloadData from "../testData/create_event_payload.json";
import addSpeakerPayloadData from "../testData/add_speaker_payload.json";
import addAttendeePayloadData from "../testData/add_attendee_payload.json";

import { CreateEventParameters } from "../interfaces/createEvent";
import EventEntryType from "../enums/EventEnums";
import { EventLandingPageOptions } from "../interfaces/eventLandingPageOptions";
import BaseAPIController, { generateUniqueString } from "./BaseController";


 class EventsController extends BaseAPIController {
  constructor() {
    super();
  }

  async createNewEvent({
    eventTitle,
    eventDescription = "This is a test event",
    teamId = null,
    defaultSettings = true,
    sameDayEvent = false,
    timezone = "Asia/Kolkata",
    deltaStartDays = 1,
    deltaEndDays = 3,
    eventFormat = "HYBRID",
  }: CreateEventParameters): Promise<[string, string]> {
    try {
      const [startDate, endDate] = await this.getEventPayloadCreationDates(
        sameDayEvent,
        defaultSettings,
        deltaStartDays,
        deltaEndDays
      );
      eventTitle = `${eventTitle}-${generateUniqueString()}`;
      createEventPayloadData.title = eventTitle;
      createEventPayloadData.tz = timezone;
      createEventPayloadData.startDateTime = startDate;
      createEventPayloadData.endDateTime = endDate;
      createEventPayloadData.eventFormat = eventFormat;
      createEventPayloadData.description = eventDescription;
      createEventPayloadData.teamId =
        teamId !== null ? teamId : await this.getDefaultTeamIdForOrg();
      createEventPayloadData.isFlex = true;
      createEventPayloadData.seriesEventId = null;

      const response: AxiosResponse = await this.post(
        "/event/",
        createEventPayloadData
      );

      if (response.status !== 200) {
        throw new Error(`API to create event failed with ${response.status}`);
      }
      console.log("Event data -> ", response.data);

      const eventName = response.data.title;
      const eventId = response.data.eventId;

      console.log("#######################");
      console.log(`Event Id -> ${eventId} and Event Name -> ${eventName}`);
      console.log("#######################");

      return [eventId, eventName];
    } catch (error) {
      console.error("Error creating event:", error);
      throw error;
    }
  }

  private async getLandingPageDetails(eventId: Number | string) {
    console.log(`getting landing page detauils for ${eventId}`);
    const response = await this.get(`/event/landing_page/${eventId}`);

    if (response.status != 200) {
      console.log(`API failed with ${response.status}`);
    }
    return response.data;
  }

  private async updateEventLandingPageDetails(
    eventId: string,
    {
      eventEntryType = EventEntryType.REG_BASED,
      isMagicLinkEnabled = true,
      isSpeakermagicLinkEnabled = true,
      isGuestAuthEnabled = true,
      speakerAuthOptions = [
        "EMAIL",
        "EMAIL_OTP",
        "SSO",
        "FACEBOOK",
        "LINKEDIN",
        "GOOGLE",
      ],
      attendeeAuthOptions = [
        "EMAIL",
        "EMAIL_OTP",
        "SSO",
        "FACEBOOK",
        "LINKEDIN",
        "GOOGLE",
      ],
    }: EventLandingPageOptions
  ) {
    let landingPageData = await this.getLandingPageDetails(eventId);
    let landingPageId = landingPageData.eventLandingPageId;

    landingPageData.eventEntryType = eventEntryType;
    landingPageData.guestModeEnabled = isGuestAuthEnabled;
    landingPageData.magicLinkEnabled = isMagicLinkEnabled;
    landingPageData.attendeeAuthType = attendeeAuthOptions;
    landingPageData.speakerAuthType = speakerAuthOptions;
    landingPageData.speakerMagicLinkEnabled = isSpeakermagicLinkEnabled;

    console.log("Payload to update settings", landingPageData);

    const updateLandingPageResponse = await this.patch(
      `/event/landing_page/${eventId}/${landingPageId}`,
      landingPageData
    );

    if (updateLandingPageResponse.status != 200) {
      throw new Error(
        `API to update landing page failed with ${
          updateLandingPageResponse.status
        }, with message ${await updateLandingPageResponse.data}`
      );
    }

    console.log(
      "Updated landing page details",
      await updateLandingPageResponse.data
    );
  }

  async enableOtpLogin(
    eventId: string,
    attendeeMagicLink: boolean = true,
    speakerMagicLink: boolean = true
  ) {
    let speakerAuthOptions = ["EMAIL_OTP"];
    speakerAuthOptions.push("EMAIL");

    let AttendeeAuthOptions = ["EMAIL_OTP"];
    speakerAuthOptions.push("EMAIL");

    await this.updateEventLandingPageDetails(eventId, {
      eventEntryType: EventEntryType.REG_BASED,
      isMagicLinkEnabled: attendeeMagicLink,
      isSpeakermagicLinkEnabled: speakerMagicLink,
      isGuestAuthEnabled: false,
      speakerAuthOptions: speakerAuthOptions,
      attendeeAuthOptions: AttendeeAuthOptions,
    });

    return eventId;
  }

  async getAccountDetailsForLoggedInContext() {
    try {
      const response: AxiosResponse = await this.get("/account/details");

      if (response.status !== 200) {
        throw new Error(
          `API to get account detail failed with ${response.status}`
        );
      }

      console.log("Account details -> ", response.data);
      return response.data;
    } catch (error) {
      console.error("Error getting account details:", error);
      throw error;
    }
  }

  async getDefaultTeamIdForOrg() {
    try {
      const accountDetailsData =
        await this.getAccountDetailsForLoggedInContext();
      console.log("Account details data received -> ", accountDetailsData);

      const organisationId = accountDetailsData.organizationId;

      const listOfTeamsData = await this.fetchListOfTeams(organisationId);
      const myTeams = listOfTeamsData.myTeams;

      const generalTeam = myTeams.find(
        (team: { general: boolean }) => team.general === true
      );

      if (!generalTeam) {
        throw new Error("No general team found in list of teams for this org");
      }

      return generalTeam.teamId;
    } catch (error) {
      console.error("Error getting default team:", error);
      throw error;
    }
  }

  async fetchListOfTeams(organisationId: string) {
    try {
      const response: AxiosResponse = await this.get("/core/organization/team");

      if (response.status !== 200) {
        throw new Error(
          `API to get list of team for orgid: ${organisationId} failed with ${response.status}`
        );
      }

      console.log("List of teams -> ", response.data);
      return response.data;
    } catch (error) {
      console.error("Error getting list of teams for organisation:", error);
      throw error;
    }
  }

  async cancelEvent(eventId: string) {
    try {
      const response: AxiosResponse = await this.post(
        `/event/${eventId}/cancel`
      );

      if (response.status !== 200) {
        throw new Error(
          `Failed to cancel the event (${eventId}) with status: ${response.status}`
        );
      } else {
        console.log(
          `Event (${eventId}) canceled successfully with status: ${response.status}`
        );
      }
    } catch (error) {
      console.error(`Error cancelling event (${eventId}):`, error);
      throw error;
    }
  }

  async getEventPayloadCreationDates(
    sameDayEvent: boolean,
    defaultSettings: boolean,
    deltaStartDays: number,
    deltaEndDays: number
  ): Promise<[string, string]> {
    const currentDate = new Date();

    let startDate: string;
    let endDate: string;

    if (sameDayEvent) {
      startDate = currentDate.toISOString();
      console.log(`Event start date is ${startDate}`);
      endDate = new Date(
        currentDate.getTime() + 6 * 60 * 60 * 1000
      ).toISOString();
      console.log(`Event end date is ${endDate}`);
    } else {
      if (defaultSettings) {
        startDate = currentDate.toISOString();
        console.log(`Event start date is ${startDate}`);
        endDate = new Date(
          currentDate.getTime() + 3 * 24 * 60 * 60 * 1000
        ).toISOString();
        console.log(`Event end date is ${endDate}`);
      } else {
        startDate = new Date(
          currentDate.getTime() + deltaStartDays * 24 * 60 * 60 * 1000
        ).toISOString();
        console.log(`Event start date is ${startDate}`);
        endDate = new Date(
          currentDate.getTime() + deltaEndDays * 24 * 60 * 60 * 1000
        ).toISOString();
        console.log(`Event end date is ${endDate}`);
      }
    }

    return [startDate, endDate];
  }

  async inviteAttendee(
    eventId: string | number,
    attendeeEmail: string,
    firstName = "MobileAutomation",
    lastName = "TestAttendee",
    canSendEmail: boolean = true
  ) {
    addAttendeePayloadData.email = attendeeEmail;
    addAttendeePayloadData.firstName = firstName;
    addAttendeePayloadData.lastName = lastName;
    addAttendeePayloadData.canSendConfirmationMail = canSendEmail;
    let flowId = await this.fetchFlowId(eventId);
    addAttendeePayloadData.flowId = flowId;

    console.log(
      `Trying to add Attendee -> ${attendeeEmail} to Event -> ${eventId}`
    );

    let response = await this.post(
      `/attendee/${eventId}/invite/v3?canSendMail=${canSendEmail}`,
      addAttendeePayloadData
    );

    if (response.status === 200) {
      console.log("Attendee added successfully");
    }

    if (response.status != 200) {
      throw new Error(
        `API call to invite attendee ${attendeeEmail} failed with ${response.status}`
      );
    }
  }

  async fetchFlowId(eventId: string | number) {
    console.log(`Get flow list for event -> ${eventId}`);
    const response = await this.get(`/event/${eventId}/flow/published/list`);
    const flowList = response.data;
    console.log(`The access group list -> ${JSON.stringify(flowList)}`);
    let flowId = flowList[0].flowId;
    console.log(`The default flow id -> ${flowId}`);
    return flowId;
  }

  async fetchAttendeeAccessGroupID(eventId: string | number) {
    console.log(`Trying to find attendee accessgroup id for event ${eventId}`);
    const accessGroupListResponse = await this.get(
      `/access_groups/${eventId}/groups`
    );

    const groupList = accessGroupListResponse.data;
    console.log(`The access group list -> ${JSON.stringify(groupList)}`);

    let groupId;
    for (let group in groupList) {
      let accessGroup = groupList[group];
      if (accessGroup.groupName == "Attendees") {
        groupId = accessGroup.groupId;
        break;
      }
    }

    console.log(`The access group id for attendee group is ${groupId}`);
    return groupId;
  }

  async inviteSpeaker(
    eventId: string | number,
    speakerEmailId: string,
    speakerFirstName: string = "MobileAutomation",
    speakerLastName: string = "TestSpeaker",
    canSendEmail: boolean = true
  ) {
    addSpeakerPayloadData.firstName = speakerFirstName;
    addSpeakerPayloadData.lastName = speakerLastName;
    addSpeakerPayloadData.email = speakerEmailId;
    addSpeakerPayloadData.ticketTagType = "HYBRID";
    addSpeakerPayloadData.picUrl = "";
    addSpeakerPayloadData.designation = "";
    addSpeakerPayloadData.organization = "";
    addSpeakerPayloadData.bio = "";
    addSpeakerPayloadData.accessGroups = [];
    addSpeakerPayloadData.accessGroupMappings = [];
    addSpeakerPayloadData.canSendConfirmationMail = false;
    addSpeakerPayloadData.company = "";

    console.log(`Inviting Speaker:  ${addSpeakerPayloadData.email}`);

    let response = await this.post(
      `/speaker/${eventId}?canSendMail=${canSendEmail}`,
      addSpeakerPayloadData
    );

    if (response.status != 200) {
      throw new Error(
        `API call to invite speaker ${addSpeakerPayloadData.email} failed with ${response.status}`
      );
    }

    // sleep for 15 seconds to
    // await new Promise((r) => setTimeout(r, 15000));
    let speakerList = await this.getListOfSpeakersInEvent(eventId);

    // find the speaker in the list of speakers
    let speakerId = null;
    for (let speaker of speakerList) {
      if (speaker.email === addSpeakerPayloadData.email) {
        speakerId = speaker.speakerId;
        break;
      }
    }
    console.log(`Speaker Id -> ${speakerId}`);
  }

  private async getListOfSpeakersInEvent(eventId: string | number) {
    let response = await this.get(
      `/speaker/${eventId}/list/search?limit=300&offset=&q=`
    );

    if (response.status != 200) {
      throw new Error(`API to fetch speaker list failed with ${response.data} and body is 
        ${response.data}`);
    }
    return response.data;
  }
}

export default new EventsController();
