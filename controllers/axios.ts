import axios from "axios";

const HOSTNAME = "https://api.staging.zuddl.io";
const EVENT_SERVICE_URL = `${HOSTNAME}/api`;

axios.defaults.withCredentials = true;

const eventsAxiosClient = axios.create({
  baseURL: EVENT_SERVICE_URL,
  withCredentials: true,
  headers: {
    "csrf-header": "63TkxpZ4zL",
  },
});

const setAuthHeaderToClients = (authValue: string | undefined) => {
  eventsAxiosClient.interceptors.request.use((config) => {
    if (config.headers) {
      config.headers["Authorization"] = authValue;
    }
    return config;
  });
};

export { eventsAxiosClient, setAuthHeaderToClients };
export default axios;
