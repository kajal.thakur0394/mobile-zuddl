import { AxiosResponse } from "axios";
import BaseAPIController, { generateUniqueString } from "./BaseController";
import { SessionObject } from "../interfaces/sessionObject";
import { AddSessionPayload } from "../interfaces/addSession";

export class ScheduleController extends BaseAPIController {
  eventId: string;

  constructor(eventId: string) {
    super();
    this.eventId = eventId;
  }

  async addSession(sessionObj: SessionObject): Promise<any> {
    try {
      const endPoint = `/event/${this.eventId}/segment`;
      const response: AxiosResponse = await this.post(endPoint, sessionObj);

      if (response.status !== 200) {
        throw new Error(
          `API call to add new session failed with ${response.status}`
        );
      }

      console.log("Session schedule response:");
      console.log(response.data);

      return response.data.segmentId;
    } catch (error) {
      console.error("Error adding session:", error);
      throw error;
    }
  }

  async addSessionInEvent({
    title = "MobileTestSession",
    description = "Test Description",
    startDay = 0,
    startHour = 0,
    startMinute = 0,
    endDay = 0,
    endHour = 2,
    endMinute = 0,
    type = "NONE",
    refId = "",
    speakers = [],
    hiddenSegment = false,
    segmentFormat = "HYBRID",
    maxAvailableSpots = 4,
  }): Promise<[string, string]> {
    try {
      let currentDate = new Date();
      console.log(currentDate);
      title = `${title}-${generateUniqueString()}`;
      let startDate = new Date(currentDate);
      startDate.setDate(currentDate.getDate() + startDay);
      startDate.setHours(currentDate.getHours() + startHour);
      startDate.setMinutes(currentDate.getMinutes() + startMinute);
      let sd = startDate.toISOString();
      console.log(`Session start date is ${sd}`);

      let endDate = new Date(currentDate);
      endDate.setDate(currentDate.getDate() + endDay);
      endDate.setHours(currentDate.getHours() + endHour);
      endDate.setMinutes(currentDate.getMinutes() + endMinute);
      let ed = endDate.toISOString();
      console.log(`Session end date is ${ed}`);

      const addSessionJson: AddSessionPayload = {
        title,
        description,
        hiddenSegment,
        speakers,
        type,
        startDateTime: sd,
        endDateTime: ed,
        refId,
        eventId: this.eventId,
        day: "",
        segmentFormat: segmentFormat,
        eventInPersonLocationId: null,
        maxAvailableSpots: maxAvailableSpots,
      };

      const addSessionUri = `/segment/${this.eventId}/v2`;
      const response: AxiosResponse = await this.post(
        addSessionUri,
        addSessionJson
      );

      if (response.status !== 200) {
        throw new Error(
          `API call to add Session failed with ${response.status}`
        );
      }

      console.log("Session created with response -> ", response.data);

      const sessionId = response.data.segmentId;
      const sessionName = response.data.title;

      console.log("#######################");
      console.log(
        `Session Id -> ${sessionId} and Session Name -> ${sessionName}`
      );
      console.log("#######################");

      return [sessionId, sessionName];
    } catch (error) {
      console.error("Error adding session:", error);
      throw error;
    }
  }

  async deleteSession(sessionId: any) {
    try {
      const endPoint = `/segment/${this.eventId}/${sessionId}`;
      const response: AxiosResponse = await this.delete(endPoint);

      if (response.status !== 200) {
        throw new Error(
          `API call to delete session failed with ${response.status}`
        );
      }
    } catch (error) {
      console.error("Error deleting session:", error);
      throw error;
    }
  }

  async updateSession(
    sessionId: any,
    updatedSessionObj: {
      title: string;
      description: string;
      segmentId: string;
      eventId: string;
      refId: string;
      startDateTime?: string;
      endDateTime?: string;
      speakers?: string[];
      type: string;
    }
  ) {
    try {
      const endPoint = `/segment/${sessionId}`;
      const response: AxiosResponse = await this.patch(
        endPoint,
        updatedSessionObj
      );

      if (response.status !== 200) {
        throw new Error(
          `API call to update session failed with ${response.status}`
        );
      }

      return response;
    } catch (error) {
      console.error("Error updating session:", error);
      throw error;
    }
  }
}
