import * as path from "path";
import {
  androidDeviceCapabilities,
  iosDeviceCapabilities,
} from "../config/capabilities";
import loginLocators from "../lead-capture/test/locators/loginLocators.json";

export class DataUtil {
  static getLoginLocatorsDataObj() {
    if (
      process.env.PLATFORM == null ||
      process.env.PLATFORM == undefined ||
      process.env.PLATFORM == "android"
    ) {
      console.log("########## Android Platform##########");
      return loginLocators.android;
    } else if (process.env.PLATFORM == "ios") {
      console.log("########## iOS Platform##########");
      return loginLocators.ios;
    } else {
      throw Error("Error in invoking locatos data object");
    }
  }

  static getPlatform() {
    if (
      process.env.PLATFORM == null ||
      process.env.PLATFORM == undefined ||
      process.env.PLATFORM == "android"
    ) {
      console.log("########## Android Platform##########");
      return "android";
    } else if (process.env.PLATFORM == "ios") {
      console.log("########## iOS Platform##########");
      return "ios";
    } else {
      throw new Error("Error in invoking locators data object");
    }
  }

  static getCapabilities() {
    if (
      process.env.PLATFORM == null ||
      process.env.PLATFORM == undefined ||
      process.env.PLATFORM == "android"
    ) {
      //by default set to android
      console.log("########## Android Capabilities##########");
      console.log(androidDeviceCapabilities);
      return androidDeviceCapabilities;
    } else if (process.env.PLATFORM == "ios") {
      console.log("########## iOS Capabilities##########");
      console.log(iosDeviceCapabilities);
      return iosDeviceCapabilities;
    } else {
      throw Error("Error in invoking capabailities data");
    }
  }

  static getSpec() {
    let specPath;
    if (
      process.env.APP == null ||
      process.env.APP == undefined ||
      process.env.APP == "portal"
    ) {
      //by default set to android
      console.log("########## Testing For Portal APP ##########");

      specPath = path.join(process.cwd(), "portal/test/specs/**");
    } else {
      console.log("########## Testing For Lead-Capture APP ##########");
      specPath = path.join(process.cwd(), "lead-capture/test/specs/**");
    }
    return specPath;
  }
}
