const Mailosaur = require("mailosaur");

const mailasour_key = "uOhccejA7j1fo8C6";
const mailasour_server_id = "1gb4osi2";

export async function fetchUserEmail(): Promise<string> {
  const emailPrefix = "appium.test";
  const emailSuffix = "@1gb4osi2.mailosaur.net";
  const date = new Date();
  const time = date.getTime();

  return emailPrefix + time + emailSuffix;
}

export async function fetchVerificationCode(emailId: string): Promise<string> {
  let emailObject;
  let verificationCode;

  const mailosaur = new Mailosaur(mailasour_key);
  const subjectLine = "Access to Zuddl";

  let currDate = new Date();

  currDate.setMinutes(currDate.getMinutes() - 1);

  const criteria = {
    sentTo: emailId,
    subject: subjectLine,
  };

  try {
    console.log(`Trying to fetch message for ${emailId}`);

    const fetchedEmail = await mailosaur.messages.get(
      mailasour_server_id,
      criteria,
      {
        receivedAfter: currDate,
        timeout: 60000,
      }
    );

    verificationCode = fetchedEmail.html.codes[0].value;

    console.log("Fetched message is " + verificationCode);
    return verificationCode;
  } catch (e) {
    console.error("Exception while fetching reset link  " + e);
    throw new Error(
      `Exception occured while retreving the verificaton code -- ${emailId}`
    );
  } finally {
    if (emailObject) {
      await deleteEmailByObjId(emailObject);
    }
    return verificationCode;
  }
}

export async function fetchOtpFromEmail(
  emailId: string,
  subjectLine = "Access to Zuddl - Login using verification code: "
): Promise<any> {
  let fetchedEmailId;
  let otp;
  let message;

  const mailosaur = new Mailosaur(process.env.MAILASOUR_KEY);

  let currentDate = new Date();

  //subtract 5 min from cur_datetime
  currentDate.setMinutes(currentDate.getMinutes() - 5);

  const criteria = {
    subject: subjectLine,
    sentTo: emailId,
  };

  try {
    console.log(
      `Trying to find verification code email with criteria ${JSON.stringify(
        criteria
      )}`
    );

    message = await mailosaur.messages.get(
      process.env.MAILASOUR_SERVER_ID,
      criteria,
      {
        receivedAfter: currentDate,
        timeout: 60000,
      }
    );

    fetchedEmailId = message.id;
    console.log("Message ID -> ", JSON.stringify(fetchedEmailId));
    console.log("HTML -> ", JSON.stringify(message.html.code));
    console.log("Code 1 -> ", JSON.stringify(message.html.code[0]));
    console.log("HTML -> ", JSON.stringify(message.html.code[0].value));
    otp = message.html.codes[0].value;
    console.log("OTP fetched is " + otp);
    return otp;
  } catch (e: any) {
    console.error(
      `Exception occured : ${e.message} while retrieving email from mailaosaur for ${emailId}`
    );
    throw e;
  } finally {
    console.log("deleting the mailosaur email");
    if (fetchedEmailId) {
      await deleteEmailByObjId(fetchedEmailId);
    }
  }
}

export async function fetchResetPasswordLink(emailId: string): Promise<string> {
  let emailObject;
  let passwordResetLink;
  const subjectLine = "Reset Password Link";
  const mailosaur = new Mailosaur(process.env.MAILASOUR_KEY);

  let currDate = new Date();

  //subtract 5 min from cur_datetime
  currDate.setMinutes(currDate.getMinutes() - 5);

  const criteria = {
    sentTo: emailId,
    subject: subjectLine,
  };

  try {
    console.log(`Trying to fetch message for ${emailId}`);
    const fetchedEmailId = await mailosaur.messages.get(
      process.env.MAILASOUR_SERVER_ID,
      criteria,
      {
        receivedAfter: currDate,
        timeout: 60000,
      }
    );
    console.log("Fetched message is " + fetchedEmailId);
    emailObject = fetchedEmailId.id;
    let links = fetchedEmailId.html.links;
    console.log(`Total links fetched from the email is ${links.length}`);
    passwordResetLink = links[0].href;
    console.log("Reset Password Link -> " + passwordResetLink);
    return passwordResetLink;
  } catch (e) {
    console.error("Exception while fetching reset link  " + e);
    throw new Error(
      `Exception occured while retreving the reset link ${emailId}`
    );
  } finally {
    if (emailObject) {
      await deleteEmailByObjId(emailObject);
    }
    return passwordResetLink;
  }
}

async function deleteEmailByObjId(emailObjId: any) {
  const mailosaur = new Mailosaur(process.env.MAILASOUR_KEY);
  console.log("trying to delete the email in mailaosaur");
  try {
    await mailosaur.messages.del(emailObjId);
  } catch (e: any) {
    console.log(`error occured ${e.message} while trying to delete email`);
  }
}
