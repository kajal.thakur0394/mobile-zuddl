export class CommonUtils {
  /**
   * Replaces placeholders in a string with provided values.
   * @param locatorTemplate The template string containing placeholders.
   * @param replacements An object with keys matching the placeholders.
   * @returns The string with replaced values.
   */
  static replacePlaceholders(
    locatorTemplate: string,
    replacements: { [key: string]: string }
  ): string {
    return locatorTemplate.replace(
      /\${(.*?)}/g,
      (_, key) => `${replacements[key] || ""}`
    );
  }

  /**
   * Pauses execution for a specified number of milliseconds.
   * @param ms Milliseconds to pause.
   */
  static async pause(ms: number): Promise<void> {
    return new Promise((resolve) => setTimeout(resolve, ms));
  }

  /**
   * Formats the current date and time as a string.
   * @param format Format of the date (e.g., 'YYYY-MM-DD HH:mm:ss').
   * @returns Formatted date string.
   */
  static formatDateTime(format: string): string {
    const date = new Date();
    return format
      .replace("YYYY", date.getFullYear().toString())
      .replace("MM", String(date.getMonth() + 1).padStart(2, "0"))
      .replace("DD", String(date.getDate()).padStart(2, "0"))
      .replace("HH", String(date.getHours()).padStart(2, "0"))
      .replace("mm", String(date.getMinutes()).padStart(2, "0"))
      .replace("ss", String(date.getSeconds()).padStart(2, "0"));
  }

  /**
   * Generates a unique identifier string.
   * @returns Unique identifier.
   */
  static generateUniqueId(): string {
    return "xxxx-xxxx-xxxx-xxxx".replace(/[x]/g, () =>
      ((Math.random() * 16) | 0).toString(16)
    );
  }
  
  /**
   * Generates a random event title using the current timestamp.
   * @returns Random event title.
   */
  static getRandomEventTitle(): string {
    return Date.now().toString() + "_mobile";
  }
}
