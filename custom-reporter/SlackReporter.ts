// Import necessary modules and dependencies
import WDIOReporter from "@wdio/reporter";
import { IncomingWebhook } from "@slack/webhook";
import dotenv from "dotenv";

// Load environment variables from .env file
dotenv.config();

// Define Slack Webhook URL for sending messages
const SLACK_WEBHOOK_URL =
  "https://hooks.slack.com/services/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";

// Custom reporter class for integrating with Slack
class SlackReporter extends WDIOReporter {
  private webhook: IncomingWebhook;
  private totalTests: number;
  private totalSuccessCount: number;
  private totalFailureCount: number;
  private totalRetryCount: number;
  private isOnLocal: boolean;
  private testStartTime: Date;
  private testEndTime: Date;
  private platform: string;
  private app: string;
  private branch: string;
  private env: string;
  private projectName: string;

  // Constructor initializes the Slack webhook, counters, and environment variables
  constructor(options: any) {
    super(options);
    // Initialize Slack webhook with the provided URL
    this.webhook = new IncomingWebhook(SLACK_WEBHOOK_URL);

    // Initialize counters for test results
    this.totalTests = 0;
    this.totalSuccessCount = 0;
    this.totalFailureCount = 0;
    this.totalRetryCount = 0;

    // Determine if running on local environment based on environment variable
    this.isOnLocal = process.env.is_on_local === 'true';

    // Initialize timestamps for test start and end times
    this.testStartTime = new Date(); // Start time set at constructor initialization
    this.testEndTime = new Date(); // End time initially set to start time

    // Set default values for environment variables or use provided values
    this.platform = process.env.PLATFORM || "Unknown";
    this.app = process.env.APP || "Unknown";
    this.branch = process.env.BRANCH || "Unknown";
    this.env = process.env.ENV || "Unknown";
    this.projectName = "Mobile-Automation"; // Default project name
  }

  // Event handler for test pass event
  onTestPass() {
    this.totalSuccessCount += 1; // Increment success count on test pass event
  }

  // Event handler for test fail event
  onTestFail() {
    this.totalFailureCount += 1; // Increment failure count on test fail event
  }

  // Event handler for test retry event
  onTestRetry() {
    this.totalRetryCount += 1; // Increment retry count on test retry event
  }

  // Event handler for test runner end event
  onRunnerEnd() {
    // Update test end time when the runner ends
    this.testEndTime = new Date();

    // Calculate total tests executed, skipped tests, and success rate
    this.totalTests =
      this.totalSuccessCount + this.totalFailureCount + this.totalRetryCount;
    const skippedTests =
      this.totalTests - this.totalSuccessCount - this.totalFailureCount;
    const successRate =
      this.totalTests > 0
        ? Math.round((this.totalSuccessCount / this.totalTests) * 100)
        : 0;

    // Function to format time to 'Hour:Minute AM/PM' format
    const formatTime = (date: Date) =>
      date.toLocaleTimeString("en-US", {
        hour: "numeric",
        minute: "numeric",
        second: "numeric",
        hour12: true,
      });

    // Function to calculate duration between start and end times in hours, minutes, and seconds
    const getDuration = (startTime: Date, endTime: Date) => {
      const durationInSeconds = Math.abs((endTime.getTime() - startTime.getTime()) / 1000); // Calculate absolute difference and convert to seconds
      const hours = Math.floor(durationInSeconds / 3600); // Calculate hours
      const minutes = Math.floor((durationInSeconds % 3600) / 60); // Calculate minutes
      const seconds = Math.floor(durationInSeconds % 60); // Calculate seconds

      // Format hours, minutes, and seconds into a string
      const formattedDuration = `${hours}h ${minutes}m ${seconds}s`;

      return formattedDuration;
    };

    // Determine which image URL to use based on success rate
    const successImageUrl =
      "https://t4.ftcdn.net/jpg/04/61/51/21/360_F_461512195_rBzpceWlGBFjOFr2zSK1Qe5obeY5pRCQ.jpg";
    const failureImageUrl =
      "https://encrypted-tbn0.gstatic.com/images?q=tbn:ANd9GcSnjn_PeZstqoJ1HZ7T_D8ubhUOc5-R0zkouiu3HQIoTg&s";
    const selectedImageUrl =
      successRate > 80 ? successImageUrl : failureImageUrl;

    // Calculate duration between start and end time
    const duration = getDuration(this.testStartTime, this.testEndTime);

    // Slack message structure with formatted fields
    const messageBlocks = {
      blocks: [
        {
          type: "section",
          text: {
            type: "mrkdwn",
            text: `${this.projectName} Report:\n*${this.projectName} (allure link will be added in future)*`,
          },
          accessory: {
            type: "image",
            image_url: selectedImageUrl,
            alt_text: "test status",
          },
        },
        {
          type: "section",
          fields: [
            {
              type: "mrkdwn",
              text: `*Total Tests:*\n${this.totalTests}`,
            },
            {
              type: "mrkdwn",
              text: `*Pass Test:*\n${this.totalSuccessCount}`,
            },
            {
              type: "mrkdwn",
              text: `*Fail Test:*\n${this.totalFailureCount}`,
            },
            {
              type: "mrkdwn",
              text: `*Skipped Test:*\n${skippedTests}`,
            },
            {
              type: "mrkdwn",
              text: `*Success Rate:*\n${successRate}%`,
            },
            {
              type: "mrkdwn",
              text: `*Platform:*\n${this.platform}`,
            },
            {
              type: "mrkdwn",
              text: `*App:*\n${this.app}`,
            },
            {
              type: "mrkdwn",
              text: `*Environment:*\n${this.env}`,
            },
            {
              type: "mrkdwn",
              text: `*Branch:*\n${this.branch}`,
            },
            {
              type: "mrkdwn",
              text: `*Duration:*\n${duration}`,
            },
          ],
        },
      ],
    };

    // Send the Slack message if not running on local environment
    if (this.isOnLocal) {
      this.webhook
        .send(messageBlocks)
        .then(() => console.log("Slack report sent"))
        .catch((error) => console.log("Error sending Slack report", error));
    } else {
      console.log("Running on local, not sending Slack report");
    }
  }
}

// Export the SlackReporter class as the default module export
export default SlackReporter;
