import * as path from "path";

export const ANDROID_APP_PATH = path.join(
  process.cwd(),
  "app/Android/multievent-app-staging-release2.apk"
);
//app-staging-release.apk

export const IOS_APP_PATH = path.join(
  process.cwd(),
  "app/iOS/Zuddl Portal Staging.app"
);

export const MOCHA_OUTPUT_DIR = path.join(process.cwd(), "reports");
